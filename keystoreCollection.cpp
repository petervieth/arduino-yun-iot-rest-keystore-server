/*
  Arduino Yun Internet of Things REST Keystore Server
  (C) 2014 Peter Vieth
  
  START OF COPYING PERMISSION NOTICE
    This file is part of Arduino Yun Internet of Things REST Keystore Server.

    Arduino Yun Internet of Things REST Keystore Server is free software: 
	you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Arduino Yun Internet of Things REST Keystore Server is distributed 
	in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Arduino Yun Internet of Things REST Keystore Server.  
	If not, see <http://www.gnu.org/licenses/>.
 END OF COPYING PERMISSION NOTICE

*/

#include "keystoreCollection.h"

/*
  Initialize an empty linkedlist
  */
keystoreCollection::keystoreCollection(void) {
  size=0;
  first=NULL;
}

/* 
  Add an item to the front of the list
  */
void keystoreCollection::add(keystoreItem* k)
{
    size++;
    k->setNext(first);
    first = k;
}


/*
  Get an item by its key
  */
keystoreItem *keystoreCollection::get(String k)
{
    keystoreItem* current = first;
    while(current!=NULL) {
      if(current->getKey().equals(k)) 
      {
        return current;
      }
      current = current->getNext();
    }
    return NULL;
}

/*
  Get the size of the list
  */
int keystoreCollection::length(void)
{
  return size;
}

/*
  Get the first item in the list
  */
keystoreItem* keystoreCollection::getFirst(void)
{
  return first;
}
