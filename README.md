# README #

This is a basic shell of a generic IoT device. It connects to a cloud back end (Parse code provided) and does stuff periodically (you fill in the details).

### What is this repository for? ###

* Put business logic in python scripts.  Leave the Arduino code to just maintain some key value pairs which are tied to hardware.  
* Version: 0.1

### How do I get set up? ###

* Summary of set up

There are really three things to setup:
* Functions to read/write from Arduino pins
* Timers to do stuff periodically
* Python scripts to get data from the cloud and write data to the cloud.  Or a file.  Or not at all!

Read the header of the INO file and edit sections labeled *** EDIT ME ***.  The INO file sets up keys and a listener for HTTP requests to get or set key values.  The values of the keys can also be set via function pointers.  Sample code is provided for a refrigerator controller which reads from a DS1820B sensor.  

Additionally, in the WWW directory there are two scripts which read configuration and push data to the parse web service.  You need to create a Parse account and an application.  Alternatively, you can delete the contents and just read/write from a file.  But the cloud is cooler isn't it?

* Configuration

Just the API keys in getConfiguration.py and pushData.py

* Dependencies

Make sure to install necessary parse and openssl packages/modules on the Yun:<br/>

* opkg update #updates the available packages list
* opkg install distribute #it contains the easy_install command line tool
* opkg install python-openssl #adds ssl support to python
* easy_install pip #installs pip
* now need to download dgrtwo-parsepy from github, extract zip, and then put in tar.  
* transfer to arduino yun.  either copy to sd card or SFTP
* pip install ParsePy-master.tar
* Should spit out messages that installation was successful

* How to run tests

Testing is a bit funky.  You can test Python scripts on a PC, to a point.  Debugging on Arduino is really awful, which is one of the main reasons to fiddle as little as possible with the code on the Arduino.  Stick to Python!

* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact