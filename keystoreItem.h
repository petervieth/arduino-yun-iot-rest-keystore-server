/*
Arduino Yun Internet of Things REST Keystore Server
  (C) 2014 Peter Vieth
  
  START OF COPYING PERMISSION NOTICE
    This file is part of Arduino Yun Internet of Things REST Keystore Server.

    Arduino Yun Internet of Things REST Keystore Server is free software: 
	you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Arduino Yun Internet of Things REST Keystore Server is distributed 
	in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Arduino Yun Internet of Things REST Keystore Server.  
	If not, see <http://www.gnu.org/licenses/>.
 END OF COPYING PERMISSION NOTICE
*/

#ifndef keystoreItem_h
#define keystoreItem_h

#include<Arduino.h>
#include<String.h>


class keystoreItem
{
public:
    keystoreItem(void);
    keystoreItem(String,String);
    void setReadListener(String (*)(void));
    bool hasReadListener(void);
    String executeReadListener(void);
    void setWriteListener(void (*)(String));
    bool hasWriteListener(void);
    void executeWriteListener(String);
    void setValue(String);
    void setKey(String);
    String getKey(void);
    String getValue(void);
    void setConfiguration(bool);
    bool getConfiguration(void);
    void setNext(keystoreItem*);
    keystoreItem* getNext(void);
private:
    String key;
    String value;
    bool configuration;
    String (*readListener)(void); // funky C++ way of defining a pointer to a function
    keystoreItem *next;
    void (*writeListener)(String); // funky C++ way of defining a pointer to a function
};

#endif
