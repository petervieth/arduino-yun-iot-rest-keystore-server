/*
  A linked list of keystoreItems. 
  (C) 2014 Peter Vieth
  Arduino Yun Internet of Things REST Keystore Server
  
  START OF COPYING PERMISSION NOTICE
    This file is part of Arduino Yun Internet of Things REST Keystore Server.

    Arduino Yun Internet of Things REST Keystore Server is free software: 
	you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Arduino Yun Internet of Things REST Keystore Server is distributed 
	in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Arduino Yun Internet of Things REST Keystore Server.  
	If not, see <http://www.gnu.org/licenses/>.
 END OF COPYING PERMISSION NOTICE
 
*/

#ifndef keystoreCollection_h
#define keystoreCollection_h

#include"keystoreItem.h"

class keystoreCollection
{
public:
    keystoreCollection(void);
    void add(keystoreItem*);
    keystoreItem *get(String);
    //keystoreItem *at(int);
    int length(void);
    keystoreItem* getFirst(void);
private:
    int size;
    keystoreItem* first;

    //void resize(int);
};

#endif
