/*
  Arduino Yun Internet of Things REST Keystore Server
  (C) 2014 Peter Vieth
  
  START OF COPYING PERMISSION NOTICE
    This file is part of Arduino Yun Internet of Things REST Keystore Server.

    Arduino Yun Internet of Things REST Keystore Server is free software: 
	you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Arduino Yun Internet of Things REST Keystore Server is distributed 
	in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Arduino Yun Internet of Things REST Keystore Server.  
	If not, see <http://www.gnu.org/licenses/>.
 END OF COPYING PERMISSION NOTICE

 Note: code that requires editing is commented with *** EDIT ME ***
 
 This example for the Arduino Yun shows how to use the
 Bridge library to read and write from a key-value pair store
 on the board through REST-style calls. It demonstrates how
 you can create your own API when using REST style
 calls through the browser.  Please note that this is not true REST, as
 all requests use GET.  A true REST API would require POST to create objects
 and PUT to update objects.
 
 Use a connection over network to view debug output (not a serial COM port).
 
 Easiest way to test the keystore is to SSH to the Arduino Yun and type:
 curl -v -u root:XYZ 127.0.0.1/arduino/configuration/key/
  
 where XYZ is your password.  REST API must be enabled in Arduino Yun
 configuration page.
 
 The keystore is a linked list of keystore items which have the following members:
 - key (always a string)
 - value (always a string)
 - read listener (function pointer)
 - write listener (function pointer)
 
 The read listener is a function that is called when a read request is made. 
 The write listener is a function that is called when a new value is written to the key.
 You can define listeners that read/write pins and perform other calculations when 
 a request for a key is made.  Having only one read and one write listener was a decision
 made to conserve memory.  You could edit the code to use a linked list instead.
 
 Note that this keystore is NOT an arbitrary collection of key-value pairs.
 Due to the limited memory of the ATMega, all keys 
 should be defined in setup(), otherwise it would be possible to crash the ATMega by 
 filling its memory by creating new keys, either accidentally or maliciously.  

 This sketch is just a starting point for your own customized application.

 Possible commands created in this sketch:

 * "/arduino/keystore/key"         -> reads value of "key"
 * "/arduino/keystore/key/"        -> reads value of "key"
 * "/arduino/keystore/key/value"   -> sets value of "key" to "value"

Make sure to install necessary parse and openssl packages/modules on the Yun:

opkg update #updates the available packages list
opkg install distribute #it contains the easy_install command line tool
opkg install python-openssl #adds ssl support to python
easy_install pip #installs pip

now need to download dgrtwo-parsepy from github, extract zip, and then put in tar.  transfer to arduino yun.  either copy to sd card or SFTP
pip install ParsePy-master.tar
Should spit out messages that installation was successful
 */

// Turn debug mode on, more Console prints
#define DEBUG  1

#include <Bridge.h>
#include <YunServer.h>
#include <YunClient.h>
#include "MemoryFree.h"

// Optional, if using onewire temp sensor:
#include "OneWire.h"

// Sketch specific includes
#include "keystoreItem.h"
#include "keystoreCollection.h"
#include "Timer.h"



// Listen on default port 5555, the webserver on the Yun
// will forward there all the HTTP requests for us.
YunServer server;

/* Define keystore collection.  This is just a list of keystoreItems which are configured inside setup() */
keystoreCollection keystore;

/* define pins */ /* *** EDIT ME *** */
int compressorPin = 3;
int fanPin = 2;

/*
  A pointless listener example
*/
String dumbReadListener(void) {
  return String(millis());
}

/* Globals */  /* *** EDIT ME *** */
Timer compressor_timer;
Timer configuration_timer;

/* Temperature pins */ /* *** EDIT ME *** */
int tempPin1 = 5; // analog temperature pin
OneWire temperatureSensor(8); // PIN #

/* 
  Initialization
  */
void setup() {
  
  // Bridge startup
  /*** EDIT ME ***/
  pinMode(13, OUTPUT);
  pinMode(compressorPin, OUTPUT);
  pinMode(fanPin, OUTPUT);
  pinMode(4,OUTPUT);
  pinMode(5,OUTPUT);
  pinMode(6,OUTPUT);
  
  digitalWrite(13, LOW);
  /*** END OF EDIT ME ***/
  
  Bridge.begin(); // start the bridge interface
  Console.begin(); // start the network console debug interface
  //digitalWrite(13, HIGH);

  // Listen for incoming connection only from localhost
  // (no one from the external network may connect)
  server.listenOnLocalhost();
  server.begin();
  
  #if(DEBUG>0)
  while (!Console) {
    ; // wait for Console port to connect. - disabled in non-debug mode, as the program doesn't work unless you open the console - returns 500 OK to curl
  }
  #endif
  
  Console.println("Arduino Yun IoT Keystore - Start");
  
  #if(DEBUG>0)
  Console.print("Free memory: ");
  Console.print(freeMemory());
  Console.println(" bytes"); 
  #endif
  
  // Define keys 
  /*** EDIT ME ***/  
  keystore.add(new keystoreItem("temperature","-888")); 
  keystore.add(new keystoreItem("setpoint","30"));
  keystore.add(new keystoreItem("compressor","off"));
  keystore.add(new keystoreItem("compressor_min_cycle_time","900")); // seconds
  keystore.add(new keystoreItem("fan","off"));
  keystore.add(new keystoreItem("configuration_poll_time","900"));// seconds
  /*** END OF EDIT ME ***/
  
  #if(DEBUG>0)
  Console.print(keystore.length());
  Console.print(" keystore items added, free memory remaining: ");
  Console.print(freeMemory());
  Console.println(" bytes");
  #endif
  
  /*** EDIT ME ***/
  keystore.get("temperature")->setReadListener(readTemperature);
  if(keystore.get("temperature")!=NULL) {
    Console.println(    keystore.get("temperature")->executeReadListener()   );
  }
  
  
  // Get configuration from somewhere (you'll need to edit the getConfiguration.py script)
  getConfiguration();
  
  // Configure timers
  Console.println("Configuring timers...");
  compressor_timer.set_max_delay((keystore.get("compressor_min_cycle_time")->getValue().toInt())*1000); 
  compressor_timer.set();
  configuration_timer.set_max_delay((keystore.get("configuration_poll_time")->getValue().toInt())*1000); 
  configuration_timer.set();
  /*** END OF EDIT ME ***/
  
  
  Console.println("Done configuring timers.");
}

/*
	This function retrieves and saves configuration values.
	*/
void getConfiguration() {
  Console.println("Getting configuration (blocking process)...");
  Process p;		// Create a process and call it "p"

  p.runShellCommand("python /mnt/sd/arduino/www/bierbridge/getConfiguration.py 2>&1");
  while (p.running()); // block until complete
  
  while(p.available() > 0) {
    #if(DEBUG>0)
    Console.println("Got configuration.");
    #endif
    
    // Parse key value pairs provided by python script
    String k = "";
    String v = "";
    #if(DEBUG>0)
    Console.print("Read:");
    #endif
    
    if (p.available() > 0) {
      // read the key
      char c = p.read();
      #if(DEBUG>0)
      Console.print(c);
      #endif
      while(c!='=' && p.available() > 0) {
        k.concat(c);
        c = p.read();
        #if(DEBUG>0)
        Console.print(c);
        #endif
      }
      // read the value
      while(c!='\n' && p.available() > 0) {
        if(c!='=')
          v.concat(c);
        c = p.read();
        #if(DEBUG>0)
        Console.print(c);
        #endif
      }
     
      // update keystore
      if(keystore.get(k)!=NULL) {
        keystore.get(k)->setValue(v);
        keystore.get(k)->setConfiguration(true);
        Console.print("Updating ");
        Console.print(k);
        Console.print(" with value ");
        Console.println(v);
      } else {
        Console.println("Key not found");
      }
    }// if p available
  }
  return;
} // end of getConfiguration

/*
Push data to somewhere from our keystore.  Sends only keystoreitems that are non-configuration
*/
void pushData() {
  Console.println("Pushing data...");
  Process p;		// Create a process and call it "p"
  p.begin("python");	// Process that pushes data
  p.addParameter("/mnt/sd/arduino/www/bierbridge/pushData.py");
  p.addParameter("&2>1");  // redirect errour output to stdout
    
  /* Iterate through the keystore */
  keystoreItem* kv = keystore.getFirst();
  
  while(kv!=NULL) {
    if(kv->getConfiguration()==FALSE) {
      String arg = kv->getKey() + "=" + kv->getValue();
      #if (DEBUG > 0)
      Console.print("Adding ");
      Console.println(arg);
      #endif
      p.addParameter(arg);
    }
    kv=kv->getNext();
  }  
  p.run();  // run the process
  
  while(p.available()>0) {
    Console.print(p.read());
  }
  Console.println("Done spawning push data.");
} // end of pushData

/*
  The program loop
  */
void loop() {
  // Get clients coming from server
  YunClient client = server.accept();
  #if (DEBUG>0)
  Console.println("looping...");
  #endif
  // If there is a new client...
  if (client) {
    Console.println("Client connection detected!");
    // Process request
    processClientRequest(client);

    // Close connection and free resources.
    client.stop();
  }
  
  /*
    If it's time to get new configuration data and push data... 
    */
  if (configuration_timer.check()) {
    getConfiguration();
    pushData();
  }
  
  
  /*** EDIT ME ***/
  // This is a sample timer that checks if a refrigerator compressor should be on or off based on temp setpoint and min compressor cycle time.
  if (compressor_timer.check()) {
    keystore.get("temperature")->setValue(keystore.get("temperature")->executeReadListener());
    String t = keystore.get("temperature")->getValue();
    char buf[t.length()];
    t.toCharArray(buf,t.length());
    float temp = atof(buf);
    
    String s = keystore.get("setpoint")->getValue();
    char buf2[s.length()+1];
    s.toCharArray(buf2,s.length()+1);
    float setpoint = atof(buf2);
    
     // TODO: -999 means error reading temp
     
     // time has expired
     compressor_timer.set(); //reset timer
     
     if(temp > setpoint) {  
       Console.println("Turning compressor on");
       keystore.get("compressor")->setValue("on");
       keystore.get("fan")->setValue("on");
       digitalWrite(compressorPin, 1);  
       digitalWrite(fanPin, 1); 
     } else {
       Console.println("Turning compressor off");
       keystore.get("compressor")->setValue("off");
       keystore.get("fan")->setValue("off");
       digitalWrite(compressorPin, 0);  
       digitalWrite(fanPin, 0); 
     }
  }

  delay(5000); // Poll every 2.5s.  Bump this number down if you need faster responses.
  /*** END OF EDIT ME ***/
}

/*
  Process data from client request
  */
void processClientRequest(YunClient client) {
  // read the command
  Console.println("Reading...");
  String command = client.readStringUntil('/');
  
  Console.print("command=");
  Console.println(command);

  // client requesting keystore operation?
  if (command == "keystore") {
    processKeystoreRequest(client);
  }
}

/*
  Parse a request for a value from the keystore 
  */
void processKeystoreRequest(YunClient client) {
  String key = client.readStringUntil('/');
  
  // remove  whitespace
  key.trim();
  
  Console.print("Requested key is '");
  Console.print(key);
  Console.println("'");
  
  keystoreItem* item = keystore.get(key);  
  
  // If key is not found...
  if(item==NULL) {
    Console.println("Key not found in datastore");
    // Return error as JSON
    client.println(F("{\"error\":\"404\", \"reason\":\"Not found\", \"detail\":\"error - key not found in store\"}"));
    return;
  }
  
  Console.print("  with current value of '");
  Console.print(item->getValue());
  Console.println("'");
  
  if(item->hasReadListener()) {
    Console.println("Updating value using read listener...");
    item->setValue(item->executeReadListener());
  }
  
  // If the next character is a '/' it means we have an URL
  // with a value like: "/configuration/key/value" and request is trying to set
  // the key value
  if (client.available()) {
    String value = "";
    Console.println("Reading new value...");
    while(client.available()) {
      char temp = client.read();
      Console.print("adding letter ");
      Console.print(temp); // print the human readable version of the character
      Console.print("/");
      Console.println(temp, DEC); // print the ASCII character code #
      value.concat(temp);
    }
    value.trim(); // remove leading and trailing whitespace
    Console.print("Value is now ");
    Console.println(value);
    item->setValue(value); // Save new value to keystore
  }
  else {
    // Don't do anything if a read request
    Console.println("Not changing value");
  }

  // Return response as JSON
  client.print("{\"");
  client.print(key);
  client.print("\":\"");
  client.print(item->getValue());
  client.print("\"}");
}





/*
  Read temp from a OneWire temp sensor
  Returns the temperature in C x 100.  So 39.6C would be returned as 3960
  This is due to the difficulties of dealing with floats.
  */
int readOneWireTemp(OneWire ds) {
  byte i;
  byte present = 0;
  byte data[12];
  byte addr[8];

  if ( !ds.search(addr)) {
      //Serial.print("No more addresses.\n");
      ds.reset_search();
      return -999;
  }

  /*Serial.print("R=");
  for( i = 0; i < 8; i++) {
    Serial.print(addr[i], HEX);
    Serial.print(" ");
  }*/

  if ( OneWire::crc8( addr, 7) != addr[7]) {
      //Serial.print("CRC is not valid!\n");
      return -999;
  }

  /*if ( addr[0] == 0x10) {
      Serial.print("Device is a DS18S20 family device.\n");
  }
  else if ( addr[0] == 0x28) {
      Serial.print("Device is a DS18B20 family device.\n");
  }
  else {
      Serial.print("Device family is not recognized: 0x");
      Serial.println(addr[0],HEX);
      return -999;
  }*/

  ds.reset();
  ds.select(addr);
  ds.write(0x44,1);         // start conversion, with parasite power on at the end

  delay(1000);     // maybe 750ms is enough, maybe not
  // we might do a ds.depower() here, but the reset will take care of it.

  present = ds.reset();
  ds.select(addr);    
  ds.write(0xBE);         // Read Scratchpad

  /*Serial.print("P=");
  Serial.print(present,HEX);
  Serial.print(" ");*/
  for ( i = 0; i < 9; i++) {           // we need 9 bytes
    data[i] = ds.read();
    //Serial.print(data[i], HEX);
    //Serial.print(" ");
  }
  /*Serial.print(" CRC=");
  Serial.print( OneWire::crc8( data, 8), HEX);
  Serial.println();*/
  
  int HighByte, LowByte, TReading, SignBit, Tc_100, Whole, Fract;
  LowByte = data[0];
  HighByte = data[1];
  TReading = (HighByte << 8) + LowByte;
  SignBit = TReading & 0x8000;  // test most sig bit
  if (SignBit) // negative
  {
    TReading = (TReading ^ 0xffff) + 1; // 2's comp
  }
  Tc_100 = (6 * TReading) + TReading / 4;    // multiply by (100 * 0.0625) or 6.25

  Whole = Tc_100 / 100;  // separate off the whole and fractional portions
  Fract = Tc_100 % 100;


  /*if (SignBit) // If its negative
  {
     Serial.print("-");
  }
  Serial.print(Whole);
  Serial.print(".");
  if (Fract < 10)
  {
     Serial.print("0");
  }
  Serial.print(Fract);

  Serial.print("\n");*/
  return Tc_100;
}


/* Listener function for reading temperature from DS1820.  Takes the temp (which is an int of a decimal to the hundredths place, but with no decimal point) and converts to string */
String readTemperature(void) {
  String tempx100 = String(readOneWireTemp(temperatureSensor));
  String t1 = tempx100.substring(0,tempx100.length()-2);
  t1=t1+".";
  String t2 = tempx100.substring(tempx100.length()-2);
  Console.print("Read temperature of ");
  Console.println(t1+t2);
  return t1+t2;
}

