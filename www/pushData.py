#!/usr/bin/python
 
#Arduino Yun Internet of Things REST Keystore Server
#  (C) 2014 Peter Vieth
#  
#  START OF COPYING PERMISSION NOTICE
#    This file is part of Arduino Yun Internet of Things REST Keystore Server.
#
#    Arduino Yun Internet of Things REST Keystore Server is free software: 
#	you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    Arduino Yun Internet of Things REST Keystore Server is distributed 
#	in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with Arduino Yun Internet of Things REST Keystore Server.  
#	If not, see <http://www.gnu.org/licenses/>.
# END OF COPYING PERMISSION NOTICE 
 
# call using, for example, python cc\pushData.py foo1=val1 foo2=val2

import os, sys

DEBUG_PRINT=False

dataToPush = []

if len(sys.argv) > 1:
    if DEBUG_PRINT:
        print 'Number of arguments:', len(sys.argv), 'arguments.'
        print 'Argument List:', str(sys.argv)
    # iterate over the arguments and store in data to push
    for x in range(1, len(sys.argv)):
        # still need to test that the split is OK
        dataToPush.append(sys.argv[x].split("="))
    if DEBUG_PRINT:
        print dataToPush
else:
    print "Not enough arguments"
    sys.exit()

APPLICATION_ID = "APPLICATION_ID_HERE"
REST_API_KEY = "YOUR_API_KEY_HERE"

if APPLICATION_ID == "APPLICATION_ID_HERE":
    #printTitle("You need to create a parse app and supply the auth values")
    sys.exit(-1)

if DEBUG_PRINT:
    print "We will use the ParsePy library"

# Install with pip install git+https://github.com/dgrtwo/ParsePy.git

from parse_rest.connection import register, ParseBatcher
# Alias the Object type to make clear is not a normal python Object
from parse_rest.datatypes import Object as ParseObject

# import parseConfiguration

class Device(ParseObject):
        pass

class DataPoint(ParseObject):
        pass


if DEBUG_PRINT:
    print "First register the app"

register(APPLICATION_ID, REST_API_KEY)

if DEBUG_PRINT:
    print "Registered, getting mac address"

# Get mac addr
from uuid import getnode as get_mac
mac = get_mac()

if DEBUG_PRINT:
    print "MAC address: %s" % mac




# Get the current device

if DEBUG_PRINT:
    print "Querying for devices with this MAC..."

devices = Device.Query.filter(uid=mac) #.limit(1)
if DEBUG_PRINT:
    list(devices)

deviceFound = False
thisDevice = None

for device in devices:
    deviceFound = True
    
    thisDevice = device
    if DEBUG_PRINT:
        print "Device exists:"
        print thisDevice
    
if not deviceFound:
    if DEBUG_PRINT:
        print "Device not registered, registering..."
    thisDevice = Device(uid=mac,name="Bierlitzer")
    thisDevice.save()
    if DEBUG_PRINT:
        print thisDevice
        print "Registered"

if DEBUG_PRINT:
    print "Logging in.."


# Save Data

if DEBUG_PRINT:
    print "Pushing data"

class DataPoint(ParseObject):
	pass

for kvp in dataToPush:
    dp = DataPoint(device=thisDevice,key=kvp[0],value=kvp[1])
    dp.save()
    if DEBUG_PRINT:
        print "Pushed %s=%s" % (kvp[0],kvp[1])


