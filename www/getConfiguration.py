#!/usr/bin/python

#Arduino Yun Internet of Things REST Keystore Server
#  (C) 2014 Peter Vieth
#  
#  START OF COPYING PERMISSION NOTICE
#    This file is part of Arduino Yun Internet of Things REST Keystore Server.
#
#    Arduino Yun Internet of Things REST Keystore Server is free software: 
#	you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    Arduino Yun Internet of Things REST Keystore Server is distributed 
#	in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with Arduino Yun Internet of Things REST Keystore Server.  
#	If not, see <http://www.gnu.org/licenses/>.
# END OF COPYING PERMISSION NOTICE



import os, sys

DEBUG_PRINT=False

APPLICATION_ID = "APPLICATION_ID_HERE"
REST_API_KEY = "YOUR_API_KEY_HERE"

if APPLICATION_ID == "APPLICATION_ID_HERE":
    #printTitle("You need to create a parse app and supply the auth values")
    sys.exit(-1)

if DEBUG_PRINT:
    print "We will use the ParsePy library"

# Install with pip install git+https://github.com/dgrtwo/ParsePy.git

from parse_rest.connection import register, ParseBatcher
# Alias the Object type to make clear is not a normal python Object
from parse_rest.datatypes import Object as ParseObject

class Device(ParseObject):
        pass

class DeviceConfiguration(ParseObject):
        pass


if DEBUG_PRINT:
    print "First register the app"

register(APPLICATION_ID, REST_API_KEY)

if DEBUG_PRINT:
    print "Registered, getting mac address"

# Get mac addr
from uuid import getnode as get_mac
mac = get_mac()

if DEBUG_PRINT:
    print "MAC address: %s" % mac




# Get the current device

if DEBUG_PRINT:
    print "Querying for devices with this MAC..."

devices = Device.Query.filter(uid=mac) #.limit(1)
if DEBUG_PRINT:
    list(devices)

deviceFound = False
thisDevice = None

for device in devices:
    deviceFound = True
    
    thisDevice = device
    if DEBUG_PRINT:
        print "Device exists:"
        print thisDevice
    
if not deviceFound:
    if DEBUG_PRINT:
        print "Device not registered, registering..."
    thisDevice = Device(uid=mac,name="Bierlitzer")
    thisDevice.save()
    if DEBUG_PRINT:
        print thisDevice
        print "Registered"

if DEBUG_PRINT:
    print "Logging in.."


# Configuration

cfg = DeviceConfiguration.Query.filter(device=thisDevice)
if cfg.count()<1:
    if DEBUG_PRINT:
        print "Initializing configuration..."
    cfg1 = DeviceConfiguration(device=thisDevice,key="provisioned",value="no")
    cfg1.save()
    pass

if DEBUG_PRINT:
    print "Configuration:"

cfg = DeviceConfiguration.Query.filter(device=thisDevice)
for cfgitem in cfg:
    print "%s=%s" % (cfgitem.key, cfgitem.value)
    

