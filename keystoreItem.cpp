/*
Arduino Yun Internet of Things REST Keystore Server
  (C) 2014 Peter Vieth
  
  START OF COPYING PERMISSION NOTICE
    This file is part of Arduino Yun Internet of Things REST Keystore Server.

    Arduino Yun Internet of Things REST Keystore Server is free software: 
	you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Arduino Yun Internet of Things REST Keystore Server is distributed 
	in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Arduino Yun Internet of Things REST Keystore Server.  
	If not, see <http://www.gnu.org/licenses/>.
 END OF COPYING PERMISSION NOTICE
*/

#include "keystoreItem.h"


keystoreItem::keystoreItem(void) {
}

keystoreItem::keystoreItem(String k, String v) {
  key = k;
  value = v;
  configuration = false;
}

void keystoreItem::setValue(String v)
{
    value = v;
}

String keystoreItem::getValue()
{
    return value;
}

void keystoreItem::setKey(String k)
{
    key = k;
}

String keystoreItem::getKey()
{
    return key;
}

/*
  Is this a configuration value? ie, don't try to save it to the back end
  */
void keystoreItem::setConfiguration(bool c)
{
    configuration = c;
}

bool keystoreItem::getConfiguration()
{
    return configuration;
}

void keystoreItem::setReadListener(String (*newReadListener)(void))
{
    readListener = newReadListener;
}

void keystoreItem::setWriteListener(void (*newWriteListener)(String))
{
    writeListener = newWriteListener;
}

bool keystoreItem::hasReadListener(void)
{
    return (readListener!=NULL);
}

bool keystoreItem::hasWriteListener(void)
{
    return (writeListener!=NULL);
}

String keystoreItem::executeReadListener(void)
{
    return readListener();
}

void keystoreItem::executeWriteListener(String s)
{
    writeListener(s);
    return;
}

void keystoreItem::setNext(keystoreItem* k)
{
    next = k;
}

keystoreItem* keystoreItem::getNext()
{
    return next;
}
