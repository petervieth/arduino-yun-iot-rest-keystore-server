#ifndef Timer_h
#define Timer_h

// Timer class
class Timer
{
public:
    Timer(void);
    void set_max_delay(unsigned long v);
    void set(void);
    bool check(void);
private:
    unsigned long max_delay;
    unsigned long last_set;
};

#endif
